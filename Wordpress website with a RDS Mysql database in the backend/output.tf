output "web_instance_ip" {
  value = aws_instance.wordpress.public_ip
}
output "vpc_id" {
  value = aws_vpc.prod-vpc.id
}