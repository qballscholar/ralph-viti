Requirements:

Setup a S3 backend from Terraform. Follow process describe on this link: 
    https://technology.doximity.com/articles/terraform-s3-backend-best-practices

The EC2 instance should always use the latest Amazon Linux2 AMI

VPC with a public subnet for webserver and two private subnets for RDS AZ requirements.

EC2 should allow SSH traffic from everywhere

Ec2 and RDS connectivity required.

The RDS security group should only allow communication from the EC2 SG

The output of the terraform deployment should be the public IP of the instance.
 
User-data script provided to install Wordpress, MySQL, Imagick, MariaDB, and Apache Server onto AWS Linux2 EC2.
