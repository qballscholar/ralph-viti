variable "database_password" {
  type = string
}

variable "database_name" {
  type = string
}
variable "database_user" {
  type = string
}
