terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.0"
    }
  }
}

# Define the provider
provider "aws" {
  region = "us-east-1"
}
/*# Define the variables
variable "aws_access_key" {}
variable "aws_secret_key" {}
*/
# Define VPC 
resource "aws_vpc" "prod-vpc" {
  cidr_block = "192.168.0.0/16"
  # Enabling automatic hostname assigning
  enable_dns_hostnames = true
  tags = {
    Name = "main-vpc"
  }
}
output "see" {
  value = aws_vpc.prod-vpc
}
# Creating Public subnet!
resource "aws_subnet" "subnet1" {
  depends_on = [
    aws_vpc.prod-vpc
  ]

  # VPC in which subnet has to be created!
  vpc_id = aws_vpc.prod-vpc.id

  # IP Range of this subnet
  cidr_block = "192.168.0.0/24"

  # Data Center of this subnet.
  availability_zone = "us-east-1a"

  # Enabling automatic public IP assignment on instance launch!
  map_public_ip_on_launch = true

  tags = {
    Name = "Public Subnet1"
  }
}
# Creating Private subnet!
resource "aws_subnet" "subnet2" {
  depends_on = [
    aws_vpc.prod-vpc,
    aws_subnet.subnet1
  ]

  # VPC in which subnet has to be created!
  vpc_id = aws_vpc.prod-vpc.id

  # IP Range of this subnet
  cidr_block = "192.168.1.0/24"

  # Data Center of this subnet.
  availability_zone = "us-east-1b"

  tags = {
    Name = "Private Subnet1"
  }
  
}
# Creating 2nd Private subnet!
resource "aws_subnet" "subnet3" {
  depends_on = [
    aws_vpc.prod-vpc,
    aws_subnet.subnet1,
    aws_subnet.subnet2
  ]

  # VPC in which subnet has to be created!
  vpc_id = aws_vpc.prod-vpc.id

  # IP Range of this subnet
  cidr_block = "192.168.2.0/24"

  # Data Center of this subnet.
  availability_zone = "us-east-1c"

  tags = {
    Name = "Private Subnet2"
  }
  
}
resource "aws_db_subnet_group" "private" {
  name       = "main"
  subnet_ids = [aws_subnet.subnet2.id,aws_subnet.subnet3.id]

  tags = {
    Name = "My DB subnet group"
  }
}
# Creating an Internet Gateway for the VPC
resource "aws_internet_gateway" "Internet_Gateway" {
  depends_on = [
    aws_vpc.prod-vpc,
    aws_subnet.subnet1,
    aws_subnet.subnet2,
    aws_subnet.subnet3
  ]

  # VPC in which it has to be created!
  vpc_id = aws_vpc.prod-vpc.id

  tags = {
    Name = "IG-Public-&-Private-VPC"
  }
}
# Creating an Route Table for the public subnet!
resource "aws_route_table" "Public-Subnet-RT" {
  depends_on = [
    aws_vpc.prod-vpc,
    aws_internet_gateway.Internet_Gateway
  ]

  # VPC ID
  vpc_id = aws_vpc.prod-vpc.id

  # NAT Rule
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.Internet_Gateway.id
  }

  tags = {
    Name = "Route Table for Internet Gateway"
  }
}
# Creating a resource for the Route Table Association!
resource "aws_route_table_association" "RT-IG-Association" {

  depends_on = [
    aws_vpc.prod-vpc,
    aws_subnet.subnet1,
    aws_subnet.subnet2,
    aws_subnet.subnet3,
    aws_route_table.Public-Subnet-RT
  ]

  # Public Subnet ID
  subnet_id = aws_subnet.subnet1.id

  #  Route Table ID
  route_table_id = aws_route_table.Public-Subnet-RT.id
}
# Creating an Elastic IP for the NAT Gateway!
resource "aws_eip" "Nat-Gateway-EIP" {
  depends_on = [
    aws_route_table_association.RT-IG-Association
  ]
  vpc = true
}
# Creating a NAT Gateway!
resource "aws_nat_gateway" "NAT_GATEWAY" {
  depends_on = [
    aws_eip.Nat-Gateway-EIP
  ]

  # Allocating the Elastic IP to the NAT Gateway!
  allocation_id = aws_eip.Nat-Gateway-EIP.id

  # Associating it in the Public Subnet!
  subnet_id = aws_subnet.subnet1.id
  tags = {
    Name = "Nat-Gateway_Project"
  }
}
# Creating a Route Table for the Nat Gateway!
resource "aws_route_table" "NAT-Gateway-RT" {
  depends_on = [
    aws_nat_gateway.NAT_GATEWAY
  ]

  vpc_id = aws_vpc.prod-vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.NAT_GATEWAY.id
  }

  tags = {
    Name = "Route Table for NAT Gateway"
  }

}
# Creating an Route Table Association of the NAT Gateway route 
# table with the Private Subnet!
resource "aws_route_table_association" "Nat-Gateway-RT-Association" {
  depends_on = [
    aws_route_table.NAT-Gateway-RT
  ]

  #  Private Subnet ID for adding this route table to the DHCP server of Private subnet!
  subnet_id = aws_subnet.subnet2.id

  # Route Table ID
  route_table_id = aws_route_table.NAT-Gateway-RT.id
}
# Creating a Security Group for WordPress
resource "aws_security_group" "WS-SG" {

  depends_on = [
    aws_vpc.prod-vpc,
    aws_subnet.subnet1,
    aws_subnet.subnet2,
    aws_subnet.subnet3
  ]

  description = "HTTP, PING, SSH"

  # Name of the security Group!
  name = "webserver-sg"

  # VPC ID in which Security group has to be created!
  vpc_id = aws_vpc.prod-vpc.id

  # Created an inbound rule for webserver access!
  ingress {
    description = "HTTP for webserver"
    from_port   = 80
    to_port     = 80

    # Here adding tcp instead of http, because http in part of tcp only!
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Created an inbound rule for ping
  ingress {
    description = "Ping"
    from_port   = 0
    to_port     = 0
    protocol    = "ICMP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Created an inbound rule for SSH
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22

    # Here adding tcp instead of ssh, because ssh in part of tcp only!
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outward Network Traffic for the WordPress
  egress {
    description = "output from webserver"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
# Creating security group for MySQL, this will allow access only from the instances having the security group created above.
resource "aws_security_group" "MySQL-SG" {

  depends_on = [
    aws_vpc.prod-vpc,
    aws_subnet.subnet1,
    aws_subnet.subnet2,
    aws_subnet.subnet3,
    aws_security_group.WS-SG
  ]

  description = "MySQL Access only from the Webserver Instances!"
  name        = "mysql-sg"
  vpc_id      = aws_vpc.prod-vpc.id

  # Created an inbound rule for MySQL
  ingress {
    description     = "MySQL Access"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.WS-SG.id]
  }

  egress {
    description = "output from MySQL"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
# Creating security group for Bastion Host/Jump Box
resource "aws_security_group" "BH-SG" {

  depends_on = [
    aws_vpc.prod-vpc,
    aws_subnet.subnet1,
    aws_subnet.subnet2,
    aws_subnet.subnet3
  ]

  description = "MySQL Access only from the Webserver Instances!"
  name        = "bastion-host-sg"
  vpc_id      = aws_vpc.prod-vpc.id

  # Created an inbound rule for Bastion Host SSH
  ingress {
    description = "Bastion Host SG"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "output from Bastion Host"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
# Creating security group for MySQL Bastion Host Access
resource "aws_security_group" "DB-SG-SSH" {

  depends_on = [
    aws_vpc.prod-vpc,
    aws_subnet.subnet1,
    aws_subnet.subnet2,
    aws_subnet.subnet3,
    aws_security_group.BH-SG
  ]

  description = "MySQL Bastion host access for updates!"
  name        = "mysql-sg-bastion-host"
  vpc_id      = aws_vpc.prod-vpc.id

  # Created an inbound rule for MySQL Bastion Host
  ingress {
    description     = "Bastion Host SG"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.BH-SG.id]
  }

  egress {
    description = "output from MySQL BH"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
# Define the RDS instance
resource "aws_db_instance" "wordpressdb" {

  allocated_storage      = 20
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t2.micro"
  db_name                = "mydb"
  username               = "myuser"
  password               = "mypassword"
  db_subnet_group_name   = aws_db_subnet_group.private.id
  vpc_security_group_ids = [aws_security_group.MySQL-SG.id]

  tags = {
    Name = "MySQL"
  }
}
// -----------------------------------------------
// Change USERDATA varible value after grabbing RDS endpoint info
// -----------------------------------------------
data "template_file" "user_data" {
  template = file("./user-data/userdata.sh")
  vars = {
    db_username      = var.database_user
    db_user_password = var.database_password
    db_name          = var.database_name
    db_RDS           = aws_db_instance.wordpressdb.endpoint
  }

}
# Define Key Pair
resource "aws_key_pair" "deployer" {
  key_name   = "key"
  public_key = "[ssh-rsa public key] "
}
# Define the EC2 instance
resource "aws_instance" "wordpress" {
  depends_on = [
    aws_vpc.prod-vpc,
    aws_subnet.subnet1,
    aws_subnet.subnet2,
    aws_subnet.subnet3,
    aws_security_group.BH-SG,
    aws_security_group.DB-SG-SSH
  ]
  ami                    = "ami-006dcf34c09e50022"
  instance_type          = "t2.micro"
  key_name               = aws_key_pair.deployer.key_name
  vpc_security_group_ids = [aws_security_group.WS-SG.id]
  subnet_id              = aws_subnet.subnet1.id
  user_data              = data.template_file.user_data.rendered

  # Define the EBS volume for the instance
  root_block_device {
    volume_size = 20
    volume_type = "gp2"
  }

  tags = {
    Name = "wordpress"
  }
}